(function () {
    'use strict';

// Declare app level module which depends on views, and components
    angular.module('Dius', [
        'ngRoute', 'toaster', 'ngAnimate', 'ngCookies', 'mgcrea.ngStrap', 'angular.filter',  
    ]).factory('errorInterceptor', ['$q', '$location', 'toaster',
        function ($q, $location, toaster) {
            return {
                'responseError': function (rejection) {
                    // do something on error
                    if (rejection.status === 417) {
                        toaster.pop({
                            type: 'error',
                            title: "Error",
                            body: "Not Logged in",
                            showCloseButton: true
                        });
                        $location.path('/');
                    }
                    if (rejection.status === 403) {
                        toaster.pop({
                            type: 'error',
                            title: "Error",
                            body: "Not Authorized. Login in Again",
                            showCloseButton: true
                        });
                        $location.path('/');
                    }
                    if (rejection.status === 500) {
                        toaster.pop({
                            type: 'error',
                            title: "Error",
                            body: "Something Went wrong. Refresh Page.",
                            showCloseButton: true
                        });
                    }
                    return $q.reject(rejection);
                }

            }
        }]).
    config(['$routeProvider', '$locationProvider', '$httpProvider',
        function ($routeProvider, $locationProvider, $httpProvider) {
            $httpProvider.defaults.withCredentials = true;
            $httpProvider.interceptors.push('errorInterceptor');

            $routeProvider.when('/', {
                 templateUrl: 'moduleHome/home.html',
                    controller: 'HomeCtrl',
                    controllerAs: 'homeVM'
                })
                .when('/home', {
                    templateUrl: 'moduleHome/home.html',
                    controller: 'HomeCtrl',
                    controllerAs: 'homeVM'
                })
                .otherwise({redirectTo: '/'});

            $locationProvider
                .html5Mode(true);

        }]);

})();

