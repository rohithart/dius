/**
 * Created by Homeistrator on 04/12/2015.
 */
(function () {
    'use strict';
    angular.module('Dius').service('homeService', homeService);
    homeService.$inject = ['$q', '$http'];
    function homeService($q, $http) {
        var self = this;
        self.getHome = function (){
            var defer = $q.defer();
            $http({
                method: 'GET',
                url: '/api/Items/1'

            })
                .success(function (data) {
                    defer.resolve(data);
                })
                .error(function (err) {
                    defer.reject(err)
                });
            return defer.promise;
        };
    };

})();
