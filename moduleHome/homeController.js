(function () {
    'use strict';

    angular.module('Dius').controller('HomeCtrl', homeController);
    homeController.$inject = ['$location','$filter','toaster','homeService'];
    function homeController($location,$filter, toaster,homeService) {
        var homeVM = this;
        homeVM.WIP = 10;
        //init estimates
        homeVM.estimates=[1,2,3,4,5,6,7,8,9,10];
        //init all status of a card
        homeVM.status=[
            {
                Id: 1,
                name: "Starting",
                color: "#FF7F7F"
            },
            {
                Id: 2,
                name: "In Progress",
                color: "#98c3d4"
            },
            {
                Id: 3,
                name: "Done",
                color: "#7cb97c"
            }
        ];
        //init all iterations
        homeVM.iterations =[
            {
                Id: 1,
                name: "Iteration 1"
            },
            {
                Id: 2,
                name: "Iteration 2"
            }
        ];

        //init some cards to display initially

        homeVM.cards = [
            {
                Id:1,
                title:"Card1",
                description:"Card 1",
                estimate:1,
                status: {Id: 1,name: "Starting",color: "#FF7F7F"},
                Iteration: {Id: 1,name: "Iteration 1"}
            },
            {
                Id:2,
                title:"Card2",
                description:"Card 2",
                estimate:2,
                status: {Id: 2,name: "In Progress",color: "#98c3d4"},
                Iteration: {Id: 1,name: "Iteration 1"}
            },
            {
                Id:3,
                title:"Card3",
                description:"Card 3",
                estimate:4,
                status: {Id: 2,name: "In Progress",color: "#98c3d4"},
                Iteration: {Id: 1,name: "Iteration 1"}
            },
            {
                Id:4,
                title:"Card4",
                description:"Card 4",
                estimate:6,
                status: {Id: 1,name: "Starting",color: "#FF7F7F"},
                Iteration: {Id: 1,name: "Iteration 1"}
            },
            {
                Id:5,
                title:"Card5",
                description:"Card 5",
                estimate:10,
                status: {Id: 3,name: "Done",color: "#7cb97c"},
                Iteration: {Id: 1,name: "Iteration 1"}
            },
            {
                Id:6,
                title:"Card6",
                description:"Card 6",
                estimate:1,
                status: {Id: 3,name: "Done",color: "#7cb97c"},
                Iteration: {Id: 1,name: "Iteration 1"}
            },
        ];

        //create a new card
        homeVM.createNewCard = function(){
            if(homeVM.validateNewCard(homeVM.newCard)){
            //set the status as Starting by default
            var status = $filter('filter')(homeVM.status, { Id:1 })[0];
            //belongs to first iteration by default
            var iteration = $filter('filter')(homeVM.iterations, { Id:1 })[0];
            homeVM.newCard.status = status;
            homeVM.newCard.iteration = iteration;
            homeVM.cards.push(angular.copy(homeVM.newCard));
            homeVM.newCard = {};
            toaster.pop({
                        type: 'success',
                        title: "Success",
                        body: "Card Created",
                        showCloseButton: true
                    });
            }
            
        };

        //upadate status of a card
        homeVM.updateStatus = function(card) {
            var oldStatus;
            var newStatus;
            //get the previous status based on Id and new status based on name
            homeVM.status.forEach(function (r, i) {
                if (r.Id == card.status.Id) {
                    oldStatus = r;
                }
                if (r.name == card.status.name) {
                    newStatus = r;
                }
            });
            //check WIP of the card is being moved to InProgress
            if(newStatus.Id ==2) //if moving to InProgress
            {
                var inProgress = [];
                homeVM.cards.forEach(function (r, i) {
                    if (r.Id == 2) {
                        inProgress.push(r);
                    }
                });
                var total = homeVM.calculateVelocity(inProgress)+ card.estimate;
                if(total>homeVM.WIP) //if threshold is crossed, reset the old status
                {
                    toaster.pop({
                        type: 'error',
                        title: "Error",
                        body: "Exceeds WIP Limit",
                        showCloseButton: true
                    });
                    card.status = oldStatus;
                    card.oldStatus = null;
                }
                else //change status
                {
                    card.status = newStatus;
                    card.oldStatus = oldStatus
                }
            }
            else //change status
            {
                card.status = newStatus;
                card.oldStatus = oldStatus
            }

            
        };
        //valiadte a new card
        homeVM.validateNewCard = function(card){
            try {
                var message;
                if (!card.title)
                    message = "Please enter Card Title";
                if (!card.description)
                    message = "Please enter Card Description";
                if (!card.estimate)
                    message = "Please enter Card Estimate";
                
                if (message) {
                    toaster.pop({
                        type: 'error',
                        title: "Error",
                        body: message,
                        showCloseButton: true
                    });
                    return false;
                }
                else
                    return true;
            }
            catch (err) {
                toaster.pop({
                    type: 'error',
                    title: "Error",
                    body: "Select/ Enter all Fields",
                    showCloseButton: true
                });
                return false;
            }

        };
        //undo status to revert back to previous status
        homeVM.undoStatus = function(card){
            var oldStatus = angular.copy(card.status);
            card.status = angular.copy(card.oldStatus);
            card.oldStatus = angular.copy(oldStatus);

        };
        //calculate the velocity based on a supplied list of cards
        homeVM.calculateVelocity = function(cards){
            var total = 0;
            cards.forEach(function (r, i) {
               total+= r.estimate*1;
            });
            return total;
        };
        
        homeVM.init= function(){
            

        };
        homeVM.init();
    };

})();
